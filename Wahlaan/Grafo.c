#include "BusBin.h"
#include "Coloreo.h"
#include "Grafo.h"
#include "Lado.h"
#include "Lectura.h"

Grafo ConstruccionDelGrafo(void) {
    Grafo G = NULL;

  // Conseguir cantidad de Auxs y Vertices.
    Aux VyL = LeerVyL();
    if(VyL == NULL)
        return NULL;

    G = malloc(sizeof(GrafoSt));
    G->Delta = 0;
    G->Num_vertices = VyL->CoordX;
    G->Num_lados = VyL->CoordY;
    free(VyL);

    Aux *AuxsTmp = malloc(G->Num_lados * 2 * sizeof(Aux));

    for(u32 i = 0; i < G->Num_lados; i++) {
        Aux L = LeerLados();

        if(L == NULL)
            return NULL;

        AuxsTmp[i] = L;
        AuxsTmp[i+G->Num_lados] = RotarLado(L);
    }
    OrdenarLados(AuxsTmp, G->Num_lados * 2);

    G->Vertices = malloc(G->Num_vertices * 4 * sizeof(u32));
    G->Vecinos  = malloc(G->Num_vertices * sizeof(u32*));

    G->IdV = G->Num_vertices;
    G->ColorV = G->IdV * 2;
    G->GradoV = G->IdV * 3;

    u32 UltimoId = MAX_U32;
    u32 VCount = 0;
    for(u32 i = 0; i < G->Num_lados * 2; i++) {
        if(AuxsTmp[i]->CoordX == UltimoId) {
            G->Vertices[VCount+G->GradoV-1]++;

            if(G->Vertices[VCount+G->GradoV-1] > G->Delta)
                G->Delta = G->Vertices[VCount+G->GradoV-1];

        } else {
            G->Vertices[VCount+G->IdV] = AuxsTmp[i]->CoordX;
            G->Vertices[VCount+G->ColorV] = MAX_U32;
            G->Vertices[VCount] = VCount;
            UltimoId = AuxsTmp[i]->CoordX;
            G->Vertices[VCount+G->GradoV] = 1;
            VCount++;
        }
        /*
            G->Vertices[VCount+G->IdV] = AuxsTmp[i]->CoordX;
            G->Vertices[VCount+G->ColorV] = MAX_U32;
            G->Vertices[VCount] = VCount;
            UltimoId = AuxsTmp[i]->CoordX;

            u32 grado = 1;
            while(AuxsTmp[i]->CoordX == UltimoId) {
                i++;
                grado++;
            }

            G->Vertices[VCount+G->GradoV] = grado;

            if(G->Vertices[VCount+G->GradoV] > G->Delta)
                G->Delta = G->Vertices[VCount+G->GradoV];

            VCount++;
        }
        */
    }

    for(u32 i = 0; i < G->Num_vertices; i++) {
        u32 N = G->Vertices[i+G->GradoV];
        G->Vecinos[i] = malloc(N * sizeof(u32));
    }

    UltimoId = MAX_U32;
    VCount = 0;
    u32 GradoTmp = 1;
    for(u32 i = 0; i < G->Num_lados * 2; i++) {
        if(AuxsTmp[i]->CoordX == UltimoId) {
            G->Vecinos[VCount-1][GradoTmp] = AuxsTmp[i]->CoordY;
            GradoTmp++;

        } else {
            G->Vecinos[VCount][0] = AuxsTmp[i]->CoordY;
            UltimoId = AuxsTmp[i]->CoordX;
            VCount++;
             GradoTmp = 1;
        }
        free(AuxsTmp[i]);
    }
    free(AuxsTmp);

    for(u32 i = 0; i < G->Num_vertices; i++) {
        for(u32 j = 0; j < G->Vertices[i+G->GradoV]; j++) {
            u32 Pos = BusBin(G, G->Vecinos[i][j], G->Num_vertices);
            G->Vecinos[i][j] = Pos;
        }
    }
    G->Num_colores = Greedy(G);
    return G;
}

void DestruccionDelGrafo(Grafo G) {
    for(u32 i = 0; i < G->Num_vertices; i++) {
        free(G->Vecinos[i]);
    }
    free(G->Vecinos);
    free(G->Vertices);
    free(G);
}

Grafo CopiarGrafo(Grafo G) {
    Grafo H = malloc(sizeof(GrafoSt));

    H->Num_vertices = G->Num_vertices;
    H->Num_lados = G->Num_lados;
    H->Num_colores = G->Num_colores;
    H->Delta = G->Delta;
    H->IdV = G->IdV;
    H->ColorV = G->ColorV;
    H->GradoV = G->GradoV;

    H->Vertices = malloc(H->Num_vertices * 4 * sizeof(u32));

    for(u32 i = 0; i < H->Num_vertices; i++) {
        H->Vertices[i] = G->Vertices[i];
        H->Vertices[i+H->IdV] = G->Vertices[i+H->IdV];
        H->Vertices[i+H->ColorV] = G->Vertices[i+H->ColorV];
        H->Vertices[i+H->GradoV] = G->Vertices[i+H->GradoV];
    }

    H->Vecinos = malloc(H->Num_vertices * sizeof(u32*));
    for(u32 i = 0; i < H->Num_vertices; i++) {
        H->Vecinos[i] = malloc(H->Vertices[i+H->GradoV] * sizeof(u32));

        for(u32 j = 0; j < H->Vertices[i+H->GradoV]; j++) {
            H->Vecinos[i][j] = G->Vecinos[i][j];
        }
    }

    return H;
}

u32 NumeroDeVertices(Grafo G) {
    return G->Num_vertices;
}

u32 NumeroDeAuxs(Grafo G) {
    return G->Num_lados;
}

u32 NumeroDeColores(Grafo G) {
    return G->Num_colores;
}
