#include "Switch.h"

char SwitchVertices(Grafo G, u32 i, u32 j) {
    u32 Temp = G->Vertices[i];
    G->Vertices[i] = G->Vertices[j];
    G->Vertices[j] = Temp;
    return 0;
}

char SwitchColores(Grafo G, u32 i, u32 j) {
  // Comprobar i, j en rango
    if(i > G->Num_colores || j > G->Num_colores) {
        return 1;
    }

  // Recorrer vertices
    for(u32 k = 0; k < G->Num_vertices; k++) {

      // Pintar de color j los vertices pintados con color i
        if(G->Vertices[k+G->ColorV] == i) 
            G->Vertices[k+G->ColorV] = j;

      // Pintar de color i los vertices pintados con color j
        else if(G->Vertices[k+G->ColorV] == j)
            G->Vertices[k+G->ColorV] = i;
    }
    return 0;
}
