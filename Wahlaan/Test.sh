# Separamos los grafos en 4 categorias:
#  · Grafos Simples:
#   - Grafo1
#   - Grafo2
#   - Grafo3
#   - Grafo4
#   - Queen13
#   - Queen10
#   - School1
#  · Grafos Completos:
#   - K100
#   - K500
#   - K1000
#  · Grafos Grandes:
#   - Grafo5
#   - Grafo6
#   - Grafo7
#   - Grafo8
#   - Grafo9 (Tambien es Bip)
#  · Grafos Bipartitos:
#   - Grafo10
#   - Grafo11
#   - Grafo12
#   - Grafo13
#
# NECESITAS HACER MAKE ANTES DE HACER TEST.SH!

# Grafos Simples
mkdir -p LogSimples
for file in ~/Escritorio/discreta_ii_2019/grafos_discreta/testeo_obligatorio/Simples/*
do ./out.o < $file > LogSimples/$(basename $file).log 
done
for file in ~/Escritorio/discreta_ii_2019/grafos_discreta/no_obligatorio/Simples/*
do ./out.o < $file > LogSimples/$(basename $file).log 
done
# Errores en Simples
if grep -nr "ERROR" LogSimples/*
then
    printf "($(basename $file))\n"
fi

# Grafos Completos
mkdir -p LogCompletos
for file in ~/Escritorio/discreta_ii_2019/grafos_discreta/testeo_obligatorio/Completos/*
do ./out.o < $file > LogCompletos/$(basename $file).log 
done
for file in ~/Escritorio/discreta_ii_2019/grafos_discreta/no_obligatorio/Completos/*
do ./out.o < $file > LogCompletos/$(basename $file).log 
done
# Errores en Completos
if grep -nr "ERROR" LogCompletos/*
then
    printf "($(basename $file))\n"
fi

# Grafos Grandes
mkdir -p LogGrandes
for file in ~/Escritorio/discreta_ii_2019/grafos_discreta/testeo_obligatorio/Grandes/*
do ./out.o < "$file" > LogGrandes/$(basename "$file").log 
done
for file in ~/Escritorio/discreta_ii_2019/grafos_discreta/no_obligatorio/Grandes/*
do ./out.o < $file > LogGrandes/$(basename $file).log 
done
# Errores en Grandes
if grep -nr "ERROR" LogGrandes/*
then
    printf "($(basename $file))\n"
fi

# Grafos Bipartitos
mkdir -p LogBip
for file in ~/Escritorio/discreta_ii_2019/grafos_discreta/testeo_obligatorio/Bipartitos/*
do ./out.o < "$file" > LogBip/$(basename "$file").log 
done
for file in ~/Escritorio/discreta_ii_2019/grafos_discreta/no_obligatorio/Bipartitos/*
do ./out.o < $file > LogBip/$(basename $file).log 
done
# Errores en LogBip
if grep -nr "ERROR" LogBip/*
then
    printf "($(basename $file))\n"
fi
