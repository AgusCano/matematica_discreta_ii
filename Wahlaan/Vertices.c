#include "Vertices.h"

u32 NombreDelVertice(Grafo G, u32 i) {

  // Chequear i correcto
	if(i >= G->Num_vertices)
		return MAX_U32;

	u32	Id = G->Vertices[i+G->IdV];
	return Id;
}

u32 ColorDelVertice(Grafo G, u32 i) {

  // Chequear i correcto
	if(i >= G->Num_vertices)
		return MAX_U32;

	u32	CVer = G->Vertices[i+G->ColorV];
	return CVer;
}

u32 GradoDelVertice(Grafo G, u32 i) {

  // Chequear i correcto
	if(i >= G->Num_vertices)
		return MAX_U32;

	u32	Grado = G->Vertices[i+G->GradoV];
	return Grado;
}

u32 ColorJotaesimoVecino(Grafo G, u32 i, u32 j) {

  // Chequear i correcto
	if(i >= G->Num_vertices)
		return MAX_U32;

  // Chequear j correcto
	if(j >= G->Vertices[i+G->GradoV])
		return MAX_U32;

	u32	ColorJ = G->Vertices[G->Vecinos[i][j]+G->ColorV];
	return ColorJ;
}

u32 NombreJotaesimoVecino(Grafo G, u32 i, u32 j) {

  // Chequear i correcto
	if(i >= G->Num_vertices)
		return MAX_U32;

  // Chequear j correcto
	if(j >= G->Vertices[i+G->GradoV])
		return MAX_U32;

	u32	NombreJ = G->Vertices[G->Vecinos[i][j]+G->IdV];
	return NombreJ;
}
