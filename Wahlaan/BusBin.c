#include "BusBin.h"

u32 BusBinRec(Grafo G, u32 Id, u32 i, u32 j) {
    if(i <= j && j != MAX_U32) {
        u32 mid = (i + j) / 2;

        if(G->Vertices[mid+G->IdV] == Id)
            return mid;

        if(G->Vertices[mid+G->IdV] > Id) {
            return BusBinRec(G, Id, i, mid-1);

        } else {
            return BusBinRec(G, Id, mid+1, j);
        }
    } else {
        return MAX_U32;
    }
}

u32 BusBin(Grafo G, u32 Id, u32 length) {
    u32 res = BusBinRec(G, Id, 0, length);
    return res;
}
