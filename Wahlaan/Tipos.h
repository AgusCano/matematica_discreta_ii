#ifndef TIPOS_H
#define TIPOS_H

#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_U32 4294967295


typedef uint32_t u32;

typedef struct GrafoSt {
    u32 Num_vertices;
    u32 Num_colores;
    u32 Num_lados;
    u32 Delta;
    u32 IdV;
    u32 ColorV;
    u32 GradoV;
    u32 *Vertices;
    u32 **Vecinos;
} GrafoSt;

typedef GrafoSt* Grafo;

typedef struct AuxSt {
    u32 CoordX;
    u32 CoordY;
} AuxSt;

typedef AuxSt* Aux;

typedef struct ColorSt {
    u32 Count;
    u32 Size;
    u32 *PosV;
} ColorSt;

typedef ColorSt* Color;


#endif
