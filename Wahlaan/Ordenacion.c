#include "Ordenacion.h"

int Comparar(const void * c, const void * d) {
	Aux a = *(Aux*)c;
	Aux b = *(Aux*)d;

	return (int)(a->CoordY - b->CoordY);
}

int CompararR(const void * c, const void * d) {
	Aux a = *(Aux*)c;
	Aux b = *(Aux*)d;

	return (int)(b->CoordY - a->CoordY);
}

int CompararC(const void * c, const void * d) {
	Color a = *(Color*)c;
	Color b = *(Color*)d;

	return (int)(a->Count > b->Count);
}

char OrdenNatural(Grafo G) {
    for(u32 i = 0; i < G->Num_vertices; i++) {
        G->Vertices[i] = i;
    }
    return 0;
}

char OrdenWelshPowell(Grafo G) {
    Aux *A = malloc(G->Num_vertices * sizeof(Aux));

    for(u32 n = 0; n < G->Num_vertices; n++) {
        u32 i = G->Vertices[n];
        A[n] = malloc(sizeof(AuxSt));
        A[n]->CoordX = i;
        A[n]->CoordY = G->Vertices[G->GradoV+i];
    }

    qsort(A, G->Num_vertices, sizeof(Aux), &Comparar);

    for(u32 i = 0; i < G->Num_vertices; i++) {
        G->Vertices[i] = A[i]->CoordX;
        free(A[i]);
    }

    free(A);
    return 0;
}

char RMBCnormal(Grafo G) {
    Aux *A = malloc(G->Num_vertices * sizeof(Aux));

    for(u32 n = 0; n < G->Num_vertices; n++) {
        u32 i = G->Vertices[n];
        A[n] = malloc(sizeof(AuxSt));
        A[n]->CoordX = i;
        A[n]->CoordY = G->Vertices[i+G->ColorV];
    }

    qsort(A, G->Num_vertices, sizeof(Aux), &Comparar);

    for(u32 i = 0; i < G->Num_vertices; i++) {
        G->Vertices[i] = A[i]->CoordX;
        free(A[i]);
    }

    free(A);
    return 0;
}

char RMBCrevierte(Grafo G) {
    Aux *A = malloc(G->Num_vertices * sizeof(Aux));

    for(u32 n = 0; n < G->Num_vertices; n++) {
        u32 i = G->Vertices[n];
        A[n] = malloc(sizeof(AuxSt));
        A[n]->CoordX = i;
        A[n]->CoordY = G->Vertices[i+G->ColorV];
    }

    qsort(A, G->Num_vertices, sizeof(Aux), &CompararR);

    for(u32 i = 0; i < G->Num_vertices; i++) {
        G->Vertices[i] = A[i]->CoordX;
        free(A[i]);
    }

    free(A);
    return 0;
}

char RMBCchicogrande(Grafo G) {
    Color *Colores = malloc(G->Num_colores * sizeof(Color));

    for(u32 i = 0; i < G->Num_colores; i++) {
        Colores[i] = malloc(sizeof(ColorSt));
        Colores[i]->Count = 0;
        Colores[i]->Size = 1024;
        Colores[i]->PosV = malloc(Colores[i]->Size * sizeof(u32));
        if(Colores[i] == NULL || Colores[i]->PosV == NULL)
            return 1;
    }

    for(u32 n = 0; n < G->Num_vertices; n++) {
        u32 i = G->Vertices[n];
        u32 ColorA = G->Vertices[G->ColorV+i];

        if(ColorA != MAX_U32) {

            if(Colores[ColorA]->Count == Colores[ColorA]->Size) {
               Colores[ColorA]->Size += Colores[ColorA]->Size / 2;
               Colores[ColorA]->PosV = realloc(Colores[ColorA]->PosV,
                                          Colores[ColorA]->Size * sizeof(u32));
            }
            Colores[ColorA]->PosV[Colores[ColorA]->Count] = i;
            Colores[ColorA]->Count++;
        }
    }

    qsort(Colores, G->Num_colores, sizeof(Color), &CompararC);

    u32 i = 0;
    for(u32 j = 0; j < G->Num_colores; j++) {
        for(u32 k = 0; k < Colores[j]->Count; k++) {
            G->Vertices[i] = Colores[j]->PosV[k];
            i++;
        }
        free(Colores[j]->PosV);
        free(Colores[j]);
    }
    free(Colores);
    return 0;
}
