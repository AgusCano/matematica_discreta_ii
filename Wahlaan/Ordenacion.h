#ifndef ORDENACION_H
#define ORDENACION_H

#include "Tipos.h"


char OrdenNatural(Grafo G);

char OrdenWelshPowell(Grafo G);

char RMBCnormal(Grafo G);

char RMBCrevierte(Grafo G);

char RMBCchicogrande(Grafo G);


#endif
