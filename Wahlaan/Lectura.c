#include "Lectura.h"

char *ReadLine(void) {
    size_t sz = 16;
    size_t used = 0;
    char *line = NULL;

    while(1) {
        char *tmp = realloc(line, sz * sizeof(char));
        if (NULL == tmp) {
            free(line);
            return NULL;
        }
        line = tmp;

        if (NULL == fgets(line + used, (int)(sz - used), stdin)) {
            free(line);
            return NULL;
        }

        used = strlen(line);

        if (feof(stdin))
            return line;

        if ('\n' == line[used-1]) {
            line[used-1] = '\0';
            return line;
        }

        sz += sz/2;
    }
}

Aux LeerVyL(void) {
    u32 x, y;
    char c;

    Aux L = malloc(sizeof(AuxSt));

    char *AuxTmp = ReadLine();
    if (NULL == AuxTmp) {
        free(L);
        free(AuxTmp);
        return NULL;
    }

    while(AuxTmp[0] == 'c') {
        free(AuxTmp);
        AuxTmp = ReadLine();
        if (NULL == AuxTmp) {
            free(L);
            free(AuxTmp);
            return NULL;
        }
    }

    sscanf(AuxTmp, "%c %*c %*c %*c %*c %" PRIu32 " %" PRIu32 "", &c, &x, &y);
    free(AuxTmp);

    if(c == 'p') {
        L->CoordX = x;
        L->CoordY = y;
    }
    return L;
}

Aux LeerLados(void) {
    u32 x, y;
    Aux L = malloc(sizeof(AuxSt));

    char *AuxTmp = ReadLine();
    if (NULL == AuxTmp) {
        free(AuxTmp);
        free(L);
        return NULL;
    }

    sscanf(AuxTmp, "%*c %" PRIu32 " %" PRIu32 "", &x, &y);
    free(AuxTmp);

    L->CoordX = x;
    L->CoordY = y;
    return L;
}
