# Test Grafos Simples
mkdir -p LogSimples
for file in ~/Escritorio/discreta_ii_2019/grafos_discreta/testeo_obligatorio/Simples/*
do ./out.o < $file > LogSimples/$(basename $file).log 
done
# Errores en Simples
if grep -nr "ERROR" LogSimples/*
then
    printf "($(basename $file))\n"
fi
