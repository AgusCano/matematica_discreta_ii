#include "Bfs.h"

int Bfs(Grafo G, u32 posInicial) {
    int k = 0;
    u32 CContrario = 0;

  // Reservar memoria para el arreglo que servira de cola
    u32 *Queue = malloc(G->Num_vertices * sizeof(u32));

  // Variables para dar semantica de cola al arreglo Queue
    u32 FirstOfQ = 0;
    u32 CountQ = 0;

    Queue[FirstOfQ] = G->Vertices[posInicial];
    G->Vertices[Queue[FirstOfQ]+G->ColorV] = 0;

    while(FirstOfQ < G->Num_vertices && (FirstOfQ <= CountQ)) {

      // Al tomar el primer vertice de la cola, buscar color para pintar vecinos
        if(G->Vertices[Queue[FirstOfQ]+G->ColorV] == 0) {
            CContrario = 1;

        } else {
            CContrario = 0;
        }

        for(u32 i = 0; i < G->Vertices[Queue[FirstOfQ]+G->GradoV]; i++) {

          // Si el vertice no ha sido visitado lo agregamos a la cola y pintamos
            if(G->Vertices[G->Vecinos[Queue[FirstOfQ]][i]+G->ColorV] == MAX_U32) {
                CountQ++;
                Queue[CountQ] = G->Vecinos[Queue[FirstOfQ]][i];
                G->Vertices[Queue[CountQ]+G->ColorV] = CContrario;

            } else {
              // Vertice visitado tiene mismo color que un vecino, retornar 1
                if(G->Vertices[Queue[FirstOfQ]+G->ColorV] == 
                   G->Vertices[G->Vecinos[Queue[FirstOfQ]][i]+G->ColorV]) {
                    k = 1;
                    free(Queue);
                    return k;
                }
            }
        }
        FirstOfQ++;
    }
    free(Queue);
    return k;
}
