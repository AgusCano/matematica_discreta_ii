#ifndef VERTICES_H
#define VERTICES_H

#include "Tipos.h"


u32 NombreDelVertice(Grafo G, u32 i);

u32 ColorDelVertice(Grafo G, u32 i);

u32 GradoDelVertice(Grafo G, u32 i);

u32 NombreJotaesimoVecino(Grafo G, u32 i, u32 j);

u32 ColorJotaesimoVecino(Grafo G, u32 i, u32 j);


#endif
