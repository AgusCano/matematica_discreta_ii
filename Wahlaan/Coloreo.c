#include "Bfs.h"
#include "Coloreo.h"

#include <time.h>
#include <stdio.h>



u32 Greedy(Grafo G) {
    u32 color_max = 0;
    char *colores = calloc(G->Delta+1, sizeof(char));

  // Limpiar
    for(u32 i = 0; i < G->Num_vertices; i++)
        G->Vertices[i+G->ColorV] = MAX_U32;

  // Colorear
    for(u32 n = 0; n < G->Num_vertices; n++) {
        u32 i = G->Vertices[n];

      // Buscar colores de vecinos
        for(u32 j = 0; j < G->Vertices[i+G->GradoV]; j++) {
            u32 color_actual = G->Vertices[G->Vecinos[i][j]+G->ColorV];
            if(color_actual != MAX_U32)
                colores[color_actual] = 1;
		}

      // Buscar minimo color no utilizado
      // Ir limpiando los utilizados
        u32 color_min = MAX_U32;
        for(u32 j = 0; j < G->Delta + 1; j++) {
            if(colores[j] != 0) {
                colores[j] = 0;

            } else {
                color_min = j;
                break;
            }
        }

        //time_t t = time(NULL);
      // Terminar de limpiar los utilizados
        memset(colores, 0, (G->Delta + 1) * sizeof(char));

      // Actualizar nro de colores y color del vertice
        if(color_min > color_max)
            color_max = color_min;

        G->Vertices[i+G->ColorV] = color_min;
	}
    free(colores);
    return color_max + 1;
}

int Bipartito(Grafo G) {
  // Limpiar posible coloreo previo
    for(u32 i = 0; i < G->Num_vertices; i++)
      G->Vertices[i+G->ColorV] = MAX_U32;

    int k = Bfs(G, 0);

  // Buscar en todas las componentes conexas
    for(u32 i = 0; i < G->Num_vertices; i++) {

        if(G->Vertices[i+G->ColorV] == MAX_U32) {
            k = k + Bfs(G, i);
        }
    }

    if(k == 0)
	    return 1;

    else
        return 0;
}
