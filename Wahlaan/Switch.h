#ifndef SWITCH_H
#define SWITCH_H

#include "Tipos.h"


char SwitchVertices(Grafo G, u32 i, u32 j);

char SwitchColores(Grafo G, u32 i, u32 j);


#endif
