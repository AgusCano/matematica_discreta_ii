#include "Rii.h"
#include <time.h>

int main(void) {

    time_t t, total;

    total = time(NULL);

    t = time(NULL);
    Grafo G = ConstruccionDelGrafo();

    if(G == NULL) {
        printf("\nNo existe el archivo\n");
        return 1;
    }
    printf("\n------ContruccionDelGrafo:\n");
    printf("G: %" PRIu32 " colores\n\n", G->Num_colores);
    printf("------> %ld segs\n\n", time(NULL) - t);

    printf("------Bipartito:\n");
    t = time(NULL);
    if(Bipartito(G) == 0) {
        printf("No es bipartito\n\n");
    } else {
        printf("Es bipartito\n\n");
    }
    printf("-----> %ld segs\n\n", time(NULL) - t);

    t = time(NULL);
    Grafo H = CopiarGrafo(G);
    Grafo F = CopiarGrafo(G);
    printf("------2 CopiarGrafo: %ld segs\n\n", time(NULL) - t);

    printf("------Ordenar H con WelshPowell y colorear: %ld segs\n", time(NULL) - t);
    t = time(NULL);
    OrdenWelshPowell(H);
    H->Num_colores = Greedy(H);

    printf("H: %" PRIu32 " colores\n\n", H->Num_colores);
    printf("-----> %ld segs\n\n", time(NULL) - t);

    printf("------Ordenar F con RMBCnormal y colorear:\n");
    t = time(NULL);
    RMBCnormal(F);
    F->Num_colores = Greedy(F);

    printf("F: %" PRIu32 " colores\n\n", F->Num_colores);
    printf("-----> %ld segs\n\n", time(NULL) - t);

    printf("------Elegir los mejores coloreos:\n");
    t = time(NULL);
    Grafo N, M;
    if(G->Num_colores > H->Num_colores) {
        if(G->Num_colores > F->Num_colores) {
            N = CopiarGrafo(H);
            DestruccionDelGrafo(H);
            M = CopiarGrafo(F);
            DestruccionDelGrafo(F);
            printf("H --> N:\n");
            printf("G --> M:\n\n");
            DestruccionDelGrafo(G);
        } else {
            N = CopiarGrafo(H);
            DestruccionDelGrafo(H);
            M = CopiarGrafo(G);
            DestruccionDelGrafo(G);
            printf("F --> N:\n");
            printf("G --> M:\n\n");
            DestruccionDelGrafo(F);
        }
    } else if(H->Num_colores > F->Num_colores) {
        if(H->Num_colores > G->Num_colores) {
            N = CopiarGrafo(G);
            DestruccionDelGrafo(G);
            M = CopiarGrafo(F);
            DestruccionDelGrafo(F);
            printf("G --> N:\n");
            printf("F --> M:\n\n");
            DestruccionDelGrafo(H);
        } else{
            N = CopiarGrafo(H);
            DestruccionDelGrafo(H);
            M = CopiarGrafo(F);
            DestruccionDelGrafo(F);
            printf("H --> N:\n");
            printf("F --> M:\n\n");
            DestruccionDelGrafo(G);
        }
    } else {
        N = CopiarGrafo(H);
        DestruccionDelGrafo(H);
        M = CopiarGrafo(G);
        DestruccionDelGrafo(G);
        printf("H --> N:\n");
        printf("G --> M:\n\n");
        DestruccionDelGrafo(F);
    }
    printf("-----> %ld segs\n\n", time(NULL) - t);

    printf("------Hacer 100 SV en G y en H:\n");
    t = time(NULL);
    for(u32 j = 0; j < 100; j++) {
        u32 i = (u32)rand() % N->Num_vertices;
        u32 k = (u32)rand() % N->Num_vertices;
        SwitchVertices(N, i, k);

        i = (u32)rand() % M->Num_vertices;
        k = (u32)rand() % M->Num_vertices;
        SwitchVertices(M, i, k);

        N->Num_colores = Greedy(N);
        M->Num_colores = Greedy(M);
    }

    printf("N: %" PRIu32 " colores\n", N->Num_colores);
    printf("M: %" PRIu32 " colores\n\n", M->Num_colores);
    printf("-----> %ld segs\n\n", time(NULL) - t);

    u32 Check0 = N->Num_colores;
    u32 Check1 = M->Num_colores;

    printf("------Hacer 1000 RMBC en N y en M:\n");
    t = time(NULL);
    for(u32 j = 0; j < 1000; j++) {

        RMBCrevierte(N);
        RMBCchicogrande(M);

        N->Num_colores = Greedy(N);
        M->Num_colores = Greedy(M);

		if(N->Num_colores > Check0) {
		    printf("ERROR - RMBC aumenta coloreo: %" PRIu32 "\n", j);
		    printf("N->Num_Colores: %" PRIu32 " vs Check0: %" PRIu32 "\n\n", N->Num_colores, Check0);
        }
		if(M->Num_colores > Check1) {
		    printf("ERROR - RMBC aumenta coloreo: %" PRIu32 "\n", j);
		    printf("M->Num_Colores: %" PRIu32 " vs Check0: %" PRIu32 "\n\n", M->Num_colores, Check1);
        }

        Check0 = N->Num_colores;
        Check1 = M->Num_colores;
    }

    printf("N: %" PRIu32 " colores\n", N->Num_colores);
    printf("M: %" PRIu32 " colores\n\n", M->Num_colores);
    printf("-----> %ld segs (%ld minutos)\n\n", time(NULL) - t, (time(NULL) - t)/60);

    if(N->Num_colores > Check0 || M->Num_colores > Check1)
        printf("ERROR - RMBC aumento coloreo\n\n");

    t = time(NULL);
    DestruccionDelGrafo(N);
    DestruccionDelGrafo(M);
    printf("------Destruir N y M: %ld segs\n\n", time(NULL) - t);

    printf("------Tiempo total en minutos: %ld minutos\n", (time(NULL) - total)/60);

    return 0;
}
