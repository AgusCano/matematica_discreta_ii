#include "Lado.h"

Aux RotarLado(Aux L) {
    Aux Q = malloc(sizeof(AuxSt));
    Q->CoordX = L->CoordY;
    Q->CoordY = L->CoordX;
    return Q;
}

int CompararLado(const void *c, const void *d) {
	Aux a = *(Aux*)c;
	Aux b = *(Aux*)d;

	return (a->CoordX > b->CoordX) - (a->CoordX < b->CoordX);
}

char OrdenarLados(Aux *L, u32 length) {
    qsort(L, length, sizeof(Aux), &CompararLado);
    return 0;
}
