#ifndef GRAFO_H
#define GRAFO_H

#include "Tipos.h"


Grafo ConstruccionDelGrafo(void);

void DestruccionDelGrafo(Grafo G);

Grafo CopiarGrafo(Grafo G);

u32 NumeroDeVertices(Grafo G);

u32 NumeroDeLados(Grafo G);

u32 NumeroDeColores(Grafo G);


#endif
